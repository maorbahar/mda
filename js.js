
var txtarea=document.querySelector('.txtarea');
var clnall=document.querySelector('.btn-clean-all');
var runbtn=document.querySelector(".btn-run-editor");
var runbtn1=document.querySelector(".btn-run-editor2");
var paramedic=document.querySelector(".paramedics");
var param=document.querySelector(".param");
var firstupdate=document.querySelector(".first-update");
var btnsection=document.querySelectorAll('div.buttons-after-textarea button');
var btncopy=document.querySelector(".btn-copy");
var btnwhatsapp=document.querySelector(".whatsapp");
var gmail=document.querySelectorAll(".gmail");
var buttons=document.querySelectorAll('button');
var address=["1825","1867","1828","1824","1826","1827","1829","1839","1832","1823","1831"];

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
    //email system: changing the address body and subject by text area.
    for(var i=0;i<gmail.length;i++)
    {
        (function(num){ // num gets the value of i
            gmail[num].addEventListener("click", function () {
                var subject1=txtarea.value.substring(0,txtarea.value.indexOf(".")+1);
                location.href = "mailto:?bcc="+address[num]+"mda@dr.doali.co.il&body=" + txtarea.value+"&subject="+subject1;
            })
        })(i); // num gets the value of i

    }
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

//when button click it will change the color to gray.. when clicked on clean all button it wil change all buttons to green again.
function ActiveColor(bt,is) {
    is ? bt.style.backgroundColor = "#cdcdcd" : bt.style.backgroundColor = "#cc060e"// is by default true
}

//pushing the message in textarea to whatsapp
btnwhatsapp.addEventListener("click",function () {
   // location.href = "whatsapp://send?text="+txtarea.value+"&phone=+972523696444";
    location.href="whatsapp://send?text="+txtarea.value;

})

runbtn.addEventListener("click",function(){
if(txtarea.value.indexOf(":")!=-1)
    txtarea.value='*דובר מד"א, זכי הלר:*'+txtarea.value.substring(txtarea.value.indexOf(":")+1,txtarea.value.length);
    ActiveColor(runbtn,true);
})

runbtn1.addEventListener("click",function(){
    if(txtarea.value.indexOf(":")!=-1)
        txtarea.value='*דוברות מד"א:*'+txtarea.value.substring(txtarea.value.indexOf(":")+1,txtarea.value.length);
    ActiveColor(runbtn1,true);
})

//clean textarea and change the buttons to green by methos activecolor.
clnall.addEventListener("click",function () {
    txtarea.onfocus=txtarea.value='';
    for(var i=0;i<btnsection.length;i++)
    {
        ActiveColor(btnsection[i],false);
    }

})

//copy textarea message
$(btncopy).click(function(){
    $(txtarea).select();
    document.execCommand('copy');
    ActiveColor(btncopy,true);

});

//replace text method by jquery
function replaceIt(txtarea, newtxt) {
    $(txtarea).val(
        $(txtarea).val().substring(0, txtarea.selectionStart)+
        newtxt+
        $(txtarea).val().substring(txtarea.selectionEnd)
    );
}


$(paramedic).on('touchstart click', function() {
    event.stopPropagation();
    event.preventDefault();
    replaceIt($('.txtarea')[0], 'חובשים ופראמדיקים של מד"א מעניקים במקום טיפול רפואי')
    ActiveColor(paramedic,true);

})

$(param).on('touchstart click', function() {
    event.stopPropagation();
    event.preventDefault();
    replaceIt($('.txtarea')[0], 'חובשים ופראמדיקים של מד"א מעניקים טיפול רפואי ומפנים לבי"ח')
    ActiveColor(param,true);

})

$(firstupdate).on('touchstart click', function() {
    event.stopPropagation();
    event.preventDefault();
    replaceIt($('.txtarea')[0], 'עדכון בהמשך');
    ActiveColor(firstupdate,true);
})

if(txtarea.value.indexOf("שוגרו")!=-1)
{
    var str3="שוגרו";
    str=txtarea.value.substring(0,txtarea.value.indexOf(str3));
    str2=txtarea.value.substring(txtarea.value.indexOf(str3)+str3.length);
    str2=str2.substring(str2.indexOf(",")||str2.indexOf(".")+1)
    txtaf.value=str+"חובשים ופראמדיקים של מד\"א העניקו טיפול רפואי ל-"+str2;
}
